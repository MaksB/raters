package epam.rest.client;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import epam.rest.model.CC;
import epam.rest.model.Converter;
import epam.rest.model.Currency;
import epam.rest.model.InformationObject;

public class RestClient {

	private RestTemplate rt = new RestTemplate();
	private ObjectMapper mapper = new ObjectMapper();

	private static final String GET_INFORMS_URL = "http://localhost:8080/info/informs";
	private static final String GET_INFORMS_CC_URL = "http://localhost:8080/info/inform/{currency}";
	private static final String GET_CURRENCIES_BY_DATE = "http://localhost:8080/rate/currency/{date}";
	private static final String GET_CURRENCY_BY_DATE = "http://localhost:8080/rate/currency/{date}/{cc}";
	private static final String POST_CURRENCY_BY_DATE_LISF_CC = "http://localhost:8080/rate/currency/list";
	private static final String GET_CURRENCY_BY_DATE_AND_BASE_CC = "http://localhost:8080/rate/currency/{date}/base/{cc}";
	private static final String CONVERT_CURRENCY = "http://localhost:8080/rate/convert/{base}/{secondary}/{amount}";
	private static final String GET_CURRENCY_PERIOD = "http://localhost:8080/rate/currency/period/{startDate}/{endDate}";

	public List<InformationObject> callInforms() {
		String response = rt.getForObject(GET_INFORMS_URL, String.class);
		List<InformationObject> list = null;
		try {
			list = mapper.readValue(response, new TypeReference<List<InformationObject>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	public InformationObject callInformsByCC(CC cc) {
		Map<String, CC> params = new HashMap<>();
		params.put("currency", cc);

		String response = rt.getForObject(GET_INFORMS_CC_URL, String.class, params);
		InformationObject informationObject = null;
		try {
			informationObject = mapper.readValue(response, InformationObject.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return informationObject;

	}

	public List<Currency> getCurrenciesByDate(LocalDate localDate) {
		Map<String, Object> params = new HashMap<>();
		List<Currency> list = null;
		params.put("date", localDate);
		String response = rt.getForObject(GET_CURRENCIES_BY_DATE, String.class, params);

		try {
			list = mapper.readValue(response, new TypeReference<List<Currency>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	public Currency getCurrencyByDate(LocalDate localDate, CC cc) {
		Map<String, Object> params = new HashMap<>();
		Currency currency = null;
		params.put("date", localDate);
		params.put("cc", cc);

		String response = rt.getForObject(GET_CURRENCY_BY_DATE, String.class, params);

		try {
			currency = mapper.readValue(response, Currency.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return currency;
	}

	public List<Currency> getCurrenciesByDateAndListCC(LocalDate localDate, CC... cc) {

		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		List<Currency> list = null;
		params.add("date", localDate.toString());
		params.add("cc",  Arrays.asList(cc).toString().replaceAll("\\[|\\]", ""));

		String response = rt.postForObject(POST_CURRENCY_BY_DATE_LISF_CC, params, String.class);

		try {
			list = mapper.readValue(response, new TypeReference<List<Currency>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<Currency> getCurrenciesWithBaseCC(LocalDate localDate, CC cc) {
		Map<String, Object> params = new HashMap<>();
		List<Currency> list = null;
		params.put("date", localDate);
		params.put("cc", cc);

		String response = rt.getForObject(GET_CURRENCY_BY_DATE_AND_BASE_CC, String.class, params);

		try {
			list = mapper.readValue(response, new TypeReference<List<Currency>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	public Converter convertCurrency(CC base, CC secondary, Float amount) {
		Map<String, Object> params = new HashMap<>();
		Converter converter = null;
		params.put("base", base);
		params.put("secondary", secondary);
		params.put("amount", amount);

		String response = rt.getForObject(CONVERT_CURRENCY, String.class, params);

		try {
			converter = mapper.readValue(response, Converter.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return converter;
	}

	public Map<Date, List<Currency>> getCurrenciesPeriod(LocalDate startDate, LocalDate endDate) {
		Map<String, Object> params = new HashMap<>();
		Map<Date, List<Currency>> map = null;
		params.put("startDate", startDate);
		params.put("endDate", endDate);

		String response = rt.getForObject(GET_CURRENCY_PERIOD, String.class, params);

		try {
			map = mapper.readValue(response, new TypeReference<Map<Date, List<Currency>>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;

	}

}
