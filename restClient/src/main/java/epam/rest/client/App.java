package epam.rest.client;

import java.time.LocalDate;
import epam.rest.model.CC;



public class App 
{
    public static void main( String[] args )
    {
        RestClient restClient = new RestClient();
        System.out.println("\nGet currencies by date");
        restClient.getCurrenciesByDate(LocalDate.now()).stream().forEach(System.out::println);
    	System.out.println("\nGet currencies by date and cc");
    	restClient.getCurrenciesByDateAndListCC(LocalDate.now(), CC.USD, CC.EUR).stream().forEach(System.out::println);
    	System.out.println("\nGet currencies with base and cc");
    	restClient.getCurrenciesWithBaseCC(LocalDate.now(), CC.EUR).stream().forEach(System.out::println);
    	System.out.println("\nGet currency by date");
    	System.out.println(restClient.getCurrencyByDate(LocalDate.now(), CC.SEK));
    	System.out.println("\nGet currencies period");
    	 restClient.getCurrenciesPeriod(LocalDate.of(2016, 04, 01), LocalDate.of(2016,04,20)).entrySet().stream().forEach(e -> {
    		System.out.println("Date "+e.getKey());
    		e.getValue().stream().forEach(System.out::println);
    	});;
    	System.out.println("\nConvert currency");
    	System.out.println(restClient.convertCurrency(CC.CAD, CC.BYR, 200F));
    	System.out.println("\nGet information about currency");
    	restClient.callInforms().stream().forEach(System.out::println);
    	System.out.println("\nGet information about CC.CAD");
    	System.out.println(restClient.callInformsByCC(CC.CAD));

        
        
      
   
        ;
      ;
        ;
       ;
    }
}
