package epam.rest.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Currency implements Serializable{

	/**
	 * 
	 */
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	private static final long serialVersionUID = 1L;
	private String name;
	private Integer code;
	private Date exchangedate;
	private Float rate;
	private CC cc;
	
	public Currency() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Date getExchangedate() {
		return exchangedate;
	}

	public void setExchangedate(Date exchangedate) {
		this.exchangedate = exchangedate;
	}

	public Float getRate() {
		return rate;
	}

	public void setRate(Float rate) {
		this.rate = rate;
	}

	public CC getCc() {
		return cc;
	}

	public void setCc(CC cc) {
		this.cc = cc;
	}

	@Override
	public String toString() {
		return "Currency [name=" + name + ", exchangedate="
				+ dateFormat.format(exchangedate) + ", rate=" + rate + ", cc=" + cc + "]";
	}

}
