package epam.rest.model;

public class InformationObject {

	private CC cc;
	private String name;

	public InformationObject() {
	}
	
	public InformationObject(CC cc, String name) {
		super();
		this.cc = cc;
		this.name = name;
	}

	public CC getCc() {
		return cc;
	}

	public void setCc(CC cc) {
		this.cc = cc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "InformationObject [cc=" + cc + ", name=" + name + "]";
	}
	

}
