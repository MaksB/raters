package epam.rest.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import epam.rater.model.CC;
import epam.rater.model.InformationObject;
import epam.rest.service.InformationController;
@RequestMapping("/info")
@RestController
public class InformationControllerImpl implements InformationController {

	//http://localhost:8080/info/informs
	@RequestMapping(value = "/informs")
	@Override
	public List<InformationObject> getAllInformationAboutCurrency() {
		List<InformationObject> listInformation = new ArrayList<>();
		Arrays.asList(CC.values()).stream().forEach(c -> listInformation.add(new InformationObject(c, c.getName())));
		return listInformation;
	}
	
	//http://localhost:8080/info/inform/USD
	@RequestMapping(value = "/inform/{cc}")
	@Override
	public InformationObject getInformationAboutCurrency(@PathVariable CC cc) {
		return new InformationObject(cc, cc.getName());
	}

}
