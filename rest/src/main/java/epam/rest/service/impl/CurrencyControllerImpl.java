package epam.rest.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import epam.rater.model.CC;
import epam.rater.model.Converter;
import epam.rater.model.Currency;
import epam.rater.service.RateService;
import epam.rater.service.impl.RateServiceImpl;
import epam.rest.service.CurrencyContrloller;

@RestController
@RequestMapping("/rate")
public class CurrencyControllerImpl implements CurrencyContrloller {

	private RateService rateService = new RateServiceImpl();

	// http://localhost:8080/rate/currency/2016-05-24
	@RequestMapping(value = "/currency/{date}", method = RequestMethod.GET)
	@Override
	public List<Currency> getCurrenciesByDate(@PathVariable @DateTimeFormat(iso = ISO.DATE) LocalDate date) {
		rateService.validateDatePeriod(date);
		return rateService.getAllCurrencyByDate(date);
	}

	// http://localhost:8080/rate/currency/2016-05-24/USD
	@RequestMapping(value = "/currency/{date}/{cc}", method = RequestMethod.GET)
	@Override
	public Currency getCurrencyByDate(@PathVariable @DateTimeFormat(iso = ISO.DATE) LocalDate date,
			@PathVariable CC cc) {
		rateService.validateDatePeriod(date);
		return rateService.getCurrencyByDate(date, cc);
	}

	@RequestMapping(value = "/currency/list", method = RequestMethod.POST)
	@Override
	public List<Currency> getCurrenciesByDateAndCC(
			@RequestParam(name = "date") @DateTimeFormat(iso = ISO.DATE) LocalDate date,
			@RequestParam(name = "cc") List<CC> cc) {
		rateService.validateDatePeriod(date);
		return rateService.getCurrenciesByDateAndCC(date, cc);
	}

	// http://localhost:8080/rate/currency/2016-05-24/base/USD
	@RequestMapping(value = "/currency/{date}/base/{cc}", method = RequestMethod.GET)
	@Override
	public List<Currency> getCurrenciesWithBaseCC(@PathVariable @DateTimeFormat(iso = ISO.DATE) LocalDate date,
			@PathVariable CC cc) {
		rateService.validateDatePeriod(date);
		return rateService.getAllCurrencyWithBaseCC(date, cc);
	}

	// http://localhost:8080/rate/convert/EUR/USD/100
	@RequestMapping(value = "/convert/{base}/{secondary}/{amount}", method = RequestMethod.GET)
	@Override
	public Converter convertCurrency(@PathVariable @Validated CC base, @PathVariable @Validated CC secondary,
			@PathVariable Float amount) throws Exception {
		if (amount <= 0) {
			throw new Exception("incorect amount");
		}
		return rateService.convertCurrency(base, secondary, amount);
	}

	@RequestMapping(value = "/currency/period/{startDate}/{endDate}", method = RequestMethod.GET)
	@Override
	public Map<LocalDate, List<Currency>> getCurrnecyToPeriod(
			@PathVariable @DateTimeFormat(iso = ISO.DATE) LocalDate startDate,
			@PathVariable @DateTimeFormat(iso = ISO.DATE) LocalDate endDate) {
		rateService.validateDatePeriod(startDate, endDate);
		return rateService.getCurrnecyToPeriod(startDate, endDate);
	}

}
