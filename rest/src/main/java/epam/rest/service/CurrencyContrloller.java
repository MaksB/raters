package epam.rest.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import epam.rater.model.CC;
import epam.rater.model.Converter;
import epam.rater.model.Currency;

public interface CurrencyContrloller {

	public List<Currency> getCurrenciesByDate(LocalDate localDate);
	public Currency getCurrencyByDate(LocalDate localDate, CC cc);
	public List<Currency> getCurrenciesByDateAndCC(LocalDate localDate, List<CC> cc);
	public List<Currency> getCurrenciesWithBaseCC(LocalDate localDate, CC baseCC);
	public Converter convertCurrency(CC base, CC secondary, Float amount) throws Exception;
	public Map<LocalDate, List<Currency>> getCurrnecyToPeriod(LocalDate starddate, LocalDate endDate);
}
