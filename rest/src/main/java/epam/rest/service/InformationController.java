package epam.rest.service;

import java.util.List;

import epam.rater.model.CC;
import epam.rater.model.InformationObject;

public interface InformationController {

	public List<InformationObject> getAllInformationAboutCurrency();
	public InformationObject getInformationAboutCurrency(CC cc);
}
