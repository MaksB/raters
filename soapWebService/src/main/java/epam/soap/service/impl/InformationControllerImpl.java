package epam.soap.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jws.WebService;
import javax.xml.ws.WebFault;

import epam.rater.model.CC;
import epam.rater.model.InformationObject;
import epam.soap.service.InformationController;

@WebFault
@WebService(endpointInterface = "epam.soap.service.InformationController")
public class InformationControllerImpl implements InformationController{

	@Override
	public List<InformationObject> getAllInformationAboutCurrency() {
		List<InformationObject> listInformation = new ArrayList<>();
		Arrays.asList(CC.values()).stream().forEach(c -> listInformation.add(new InformationObject(c, c.getName())));
		return listInformation;
	}
	
	@Override
	public InformationObject getInformationAboutCurrency(CC cc) {
		return new InformationObject(cc, cc.getName());
	}
}
