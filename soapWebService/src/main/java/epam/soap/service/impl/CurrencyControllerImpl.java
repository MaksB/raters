package epam.soap.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;
import javax.xml.ws.WebFault;

import epam.rater.model.CC;
import epam.rater.model.Converter;
import epam.rater.model.Currency;
import epam.rater.service.RateService;
import epam.rater.service.impl.RateServiceImpl;
import epam.soap.service.CurrencyContrloller;

@WebService(endpointInterface = "epam.soap.service.CurrencyContrloller")
@WebFault
public class CurrencyControllerImpl implements CurrencyContrloller {

	private RateService rateService = new RateServiceImpl();

	@Override
	public List<Currency> getCurrenciesByDate(LocalDate date) {
		rateService.validateDatePeriod(date);
		return rateService.getAllCurrencyByDate(date);
	}

	@Override
	public Currency getCurrencyByDate(LocalDate date, CC cc) {
		rateService.validateDatePeriod(date);
		return rateService.getCurrencyByDate(date, cc);
	}

	@Override
	public List<Currency> getCurrenciesByDateAndCC(LocalDate date, List<CC> cc) {
		rateService.validateDatePeriod(date);
		return rateService.getCurrenciesByDateAndCC(date, cc);
	}

	@Override

	public List<Currency> getCurrenciesWithBaseCC(LocalDate date, CC baseCC) {
		rateService.validateDatePeriod(date);
		return rateService.getAllCurrencyWithBaseCC(date, baseCC);
	}

	@Override

	public Converter convertCurrency(CC base, CC secondary, Float amount) throws Exception {
		if (amount <= 0) {
			throw new Exception("incorect amount");
		}
		return rateService.convertCurrency(base, secondary, amount);
	}

	@Override
	public Map<LocalDate, List<Currency>> getCurrnecyToPeriod(LocalDate starddate, LocalDate endDate) {
		rateService.validateDatePeriod(starddate, endDate);
		return rateService.getCurrnecyToPeriod(starddate, endDate);
	}

}
