package epam.soap.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import epam.rater.model.CC;
import epam.rater.model.InformationObject;

@WebService
public interface InformationController {
	@WebMethod
	@WebResult(name = "information")
	public List<InformationObject> getAllInformationAboutCurrency();
	
	@WebMethod
	@WebResult(name = "information")
	public InformationObject getInformationAboutCurrency(@WebParam(name = "cc") CC cc);
}
