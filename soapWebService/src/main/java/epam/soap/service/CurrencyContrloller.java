package epam.soap.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import epam.rater.model.CC;
import epam.rater.model.Converter;
import epam.rater.model.Currency;
import epam.soap.LocalDateXmlAdapter;
import epam.soap.MapAdapter;

@WebService
public interface CurrencyContrloller {

	@WebMethod
	@WebResult(name = "Currency")
	public List<Currency> getCurrenciesByDate(@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateXmlAdapter.class) @WebParam(name = "date") LocalDate localDate);

	@WebMethod
	@WebResult(name = "Currency")
	public Currency getCurrencyByDate(
			@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateXmlAdapter.class) @WebParam(name = "date") LocalDate localDate,
			@WebParam(name = "cc") CC cc);

	@WebMethod
	@WebResult(name = "Currency")
	public List<Currency> getCurrenciesByDateAndCC(
			@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateXmlAdapter.class) @WebParam(name = "date") LocalDate localDate,
			@WebParam(name = "cc") List<CC> cc);

	@WebMethod
	@WebResult(name = "Currency")
	public List<Currency> getCurrenciesWithBaseCC(
			@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateXmlAdapter.class) @WebParam(name = "date") LocalDate localDate,
			@WebParam(name = "cc") CC baseCC);

	@WebMethod
	@WebResult(name = "Currency")
	public Converter convertCurrency(@WebParam(name = "base") CC base, @WebParam(name = "secondary") CC secondary,
			@WebParam(name = "amount") Float amount) throws Exception;

	@WebMethod
	@XmlJavaTypeAdapter(MapAdapter.class)
	@WebResult(name = "CurrencyPeriod")
	public Map<LocalDate, List<Currency>> getCurrnecyToPeriod(
			@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateXmlAdapter.class) @WebParam(name = "start") LocalDate starddate,
			@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateXmlAdapter.class) @WebParam(name = "end") LocalDate endDate);
}
