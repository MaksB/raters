package epam.soap;

import java.time.LocalDate;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import epam.rater.model.Currency;
@XmlRootElement(name = "elemets")
public class MapElements {

	@XmlElement
	@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateXmlAdapter.class)
	public LocalDate date;
	
	@XmlElementWrapper
	@XmlElement(name = "currency")
	public List<Currency> currencies;
	
	private MapElements() {
	}

	public MapElements(LocalDate key, List<Currency> currencies) {
		this.date = key;
		this.currencies = currencies;
	}
	
	
}
