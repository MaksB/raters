package epam.soap;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import epam.rater.model.Currency;

public class MapAdapter extends XmlAdapter<MapElements[], Map<LocalDate, List<Currency>>>{
	public MapAdapter() {
	}

	@Override
	public Map<LocalDate, List<Currency>> unmarshal(MapElements[] v) throws Exception {
		 Map<LocalDate, List<Currency>> r = new TreeMap<>();
	        for (MapElements mapelement : v)
	            r.put(mapelement.date, mapelement.currencies);
	        return r;
	}

	@Override
	public MapElements[] marshal(Map<LocalDate, List<Currency>> v) throws Exception {
		 MapElements[] mapElements = new MapElements[v.size()];
	        int i = 0;
	        for (Map.Entry<LocalDate, List<Currency>> entry : v.entrySet())
	            mapElements[i++] = new MapElements(entry.getKey(), entry.getValue());
	        return mapElements;
	}
	
	

}
