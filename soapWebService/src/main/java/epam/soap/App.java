package epam.soap;

import javax.xml.ws.Endpoint;

import epam.soap.service.impl.CurrencyControllerImpl;
import epam.soap.service.impl.InformationControllerImpl;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Endpoint.publish("http://localhost:8080/rate/currency", new CurrencyControllerImpl());
       Endpoint.publish("http://localhost:8080/rate/information", new InformationControllerImpl());
       
    }
}
