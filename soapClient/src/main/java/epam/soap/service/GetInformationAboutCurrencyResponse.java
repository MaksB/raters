
package epam.soap.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getInformationAboutCurrencyResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getInformationAboutCurrencyResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="information" type="{http://service.soap.epam/}informationObject" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInformationAboutCurrencyResponse", propOrder = {
    "information"
})
public class GetInformationAboutCurrencyResponse {

    protected InformationObject information;

    /**
     * Gets the value of the information property.
     * 
     * @return
     *     possible object is
     *     {@link InformationObject }
     *     
     */
    public InformationObject getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     * 
     * @param value
     *     allowed object is
     *     {@link InformationObject }
     *     
     */
    public void setInformation(InformationObject value) {
        this.information = value;
    }

}
