
package epam.soap.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getInformationAboutCurrency complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getInformationAboutCurrency">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cc" type="{http://service.soap.epam/}cc" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInformationAboutCurrency", propOrder = {
    "cc"
})
public class GetInformationAboutCurrency {

    @XmlSchemaType(name = "string")
    protected CC cc;

    /**
     * Gets the value of the cc property.
     * 
     * @return
     *     possible object is
     *     {@link CC }
     *     
     */
    public CC getCc() {
        return cc;
    }

    /**
     * Sets the value of the cc property.
     * 
     * @param value
     *     allowed object is
     *     {@link CC }
     *     
     */
    public void setCc(CC value) {
        this.cc = value;
    }

}
