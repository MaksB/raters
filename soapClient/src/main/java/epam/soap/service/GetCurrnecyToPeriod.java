
package epam.soap.service;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for getCurrnecyToPeriod complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCurrnecyToPeriod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" minOccurs="0"/>
 *         &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCurrnecyToPeriod", propOrder = {
    "start",
    "end"
})
public class GetCurrnecyToPeriod {

	@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateXmlAdapter.class)
    protected LocalDate start;
	@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateXmlAdapter.class)
    protected LocalDate end;

    /**
     * Gets the value of the start property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public LocalDate getStart() {
        return start;
    }

    /**
     * Sets the value of the start property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setStart(LocalDate value) {
        this.start = value;
    }

    /**
     * Gets the value of the end property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public LocalDate getEnd() {
        return end;
    }

    /**
     * Sets the value of the end property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setEnd(LocalDate value) {
        this.end = value;
    }

}
