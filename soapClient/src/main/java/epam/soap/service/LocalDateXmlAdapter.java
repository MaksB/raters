package epam.soap.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

public class LocalDateXmlAdapter extends XmlAdapter<String, LocalDate> {

	private final DatatypeFactory datatypeFactory;

	public LocalDateXmlAdapter() throws DatatypeConfigurationException {
		this.datatypeFactory = DatatypeFactory.newInstance();
	}

	@Override
	public LocalDate unmarshal(String xmlDate) {
		DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
		        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
		return  LocalDate.parse(xmlDate, germanFormatter);
	}

	@Override
	public String marshal(LocalDate localDate)  {
		return localDate.toString();
	}

}
