
package epam.soap.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the epam.soap.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final String SERVICE_URL = "http://service.soap.epam/";
	private static final QName _ConvertCurrencyResponse_QNAME = new QName(SERVICE_URL, "convertCurrencyResponse");
    private static final  QName _GetCurrenciesByDateResponse_QNAME = new QName(SERVICE_URL, "getCurrenciesByDateResponse");
    private static final QName _GetCurrnecyToPeriod_QNAME = new QName(SERVICE_URL, "getCurrnecyToPeriod");
    private static final  QName _Exception_QNAME = new QName(SERVICE_URL, "Exception");
    private static final QName _ConvertCurrency_QNAME = new QName(SERVICE_URL, "convertCurrency");
    private static final QName _GetCurrenciesByDate_QNAME = new QName(SERVICE_URL, "getCurrenciesByDate");
    private static final  QName _GetCurrenciesByDateAndCC_QNAME = new QName(SERVICE_URL, "getCurrenciesByDateAndCC");
    private static final  QName _GetCurrencyByDate_QNAME = new QName(SERVICE_URL, "getCurrencyByDate");
    private static final QName _GetCurrenciesWithBaseCCResponse_QNAME = new QName(SERVICE_URL, "getCurrenciesWithBaseCCResponse");
    private static final  QName _GetCurrenciesWithBaseCC_QNAME = new QName(SERVICE_URL, "getCurrenciesWithBaseCC");
    private static final QName _Elemets_QNAME = new QName(SERVICE_URL, "elemets");
    private static final QName _GetCurrnecyToPeriodResponse_QNAME = new QName(SERVICE_URL, "getCurrnecyToPeriodResponse");
    private static final QName _GetCurrenciesByDateAndCCResponse_QNAME = new QName(SERVICE_URL, "getCurrenciesByDateAndCCResponse");
    private static final QName _GetCurrencyByDateResponse_QNAME = new QName(SERVICE_URL, "getCurrencyByDateResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: epam.soap.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MapElements }
     * 
     */
    public MapElements createMapElements() {
        return new MapElements();
    }

    /**
     * Create an instance of {@link GetCurrenciesByDateResponse }
     * 
     */
    public GetCurrenciesByDateResponse createGetCurrenciesByDateResponse() {
        return new GetCurrenciesByDateResponse();
    }

    /**
     * Create an instance of {@link GetCurrnecyToPeriod }
     * 
     */
    public GetCurrnecyToPeriod createGetCurrnecyToPeriod() {
        return new GetCurrnecyToPeriod();
    }

    /**
     * Create an instance of {@link ConvertCurrencyResponse }
     * 
     */
    public ConvertCurrencyResponse createConvertCurrencyResponse() {
        return new ConvertCurrencyResponse();
    }

    /**
     * Create an instance of {@link GetCurrenciesWithBaseCCResponse }
     * 
     */
    public GetCurrenciesWithBaseCCResponse createGetCurrenciesWithBaseCCResponse() {
        return new GetCurrenciesWithBaseCCResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ConvertCurrency }
     * 
     */
    public ConvertCurrency createConvertCurrency() {
        return new ConvertCurrency();
    }

    /**
     * Create an instance of {@link GetCurrenciesByDate }
     * 
     */
    public GetCurrenciesByDate createGetCurrenciesByDate() {
        return new GetCurrenciesByDate();
    }

    /**
     * Create an instance of {@link GetCurrenciesByDateAndCC }
     * 
     */
    public GetCurrenciesByDateAndCC createGetCurrenciesByDateAndCC() {
        return new GetCurrenciesByDateAndCC();
    }

    /**
     * Create an instance of {@link GetCurrencyByDate }
     * 
     */
    public GetCurrencyByDate createGetCurrencyByDate() {
        return new GetCurrencyByDate();
    }

    /**
     * Create an instance of {@link GetCurrnecyToPeriodResponse }
     * 
     */
    public GetCurrnecyToPeriodResponse createGetCurrnecyToPeriodResponse() {
        return new GetCurrnecyToPeriodResponse();
    }

    /**
     * Create an instance of {@link GetCurrenciesWithBaseCC }
     * 
     */
    public GetCurrenciesWithBaseCC createGetCurrenciesWithBaseCC() {
        return new GetCurrenciesWithBaseCC();
    }

    /**
     * Create an instance of {@link GetCurrencyByDateResponse }
     * 
     */
    public GetCurrencyByDateResponse createGetCurrencyByDateResponse() {
        return new GetCurrencyByDateResponse();
    }

    /**
     * Create an instance of {@link GetCurrenciesByDateAndCCResponse }
     * 
     */
    public GetCurrenciesByDateAndCCResponse createGetCurrenciesByDateAndCCResponse() {
        return new GetCurrenciesByDateAndCCResponse();
    }

    /**
     * Create an instance of {@link MapElementsArray }
     * 
     */
    public MapElementsArray createMapElementsArray() {
        return new MapElementsArray();
    }

    /**
     * Create an instance of {@link Converter }
     * 
     */
    public Converter createConverter() {
        return new Converter();
    }

    /**
     * Create an instance of {@link Currency }
     * 
     */
    public Currency createCurrency() {
        return new Currency();
    }

    /**
     * Create an instance of {@link MapElements.Currencies }
     * 
     */
    public MapElements.Currencies createMapElementsCurrencies() {
        return new MapElements.Currencies();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertCurrencyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "convertCurrencyResponse")
    public JAXBElement<ConvertCurrencyResponse> createConvertCurrencyResponse(ConvertCurrencyResponse value) {
        return new JAXBElement<ConvertCurrencyResponse>(_ConvertCurrencyResponse_QNAME, ConvertCurrencyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrenciesByDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrenciesByDateResponse")
    public JAXBElement<GetCurrenciesByDateResponse> createGetCurrenciesByDateResponse(GetCurrenciesByDateResponse value) {
        return new JAXBElement<GetCurrenciesByDateResponse>(_GetCurrenciesByDateResponse_QNAME, GetCurrenciesByDateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrnecyToPeriod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrnecyToPeriod")
    public JAXBElement<GetCurrnecyToPeriod> createGetCurrnecyToPeriod(GetCurrnecyToPeriod value) {
        return new JAXBElement<GetCurrnecyToPeriod>(_GetCurrnecyToPeriod_QNAME, GetCurrnecyToPeriod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertCurrency }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "convertCurrency")
    public JAXBElement<ConvertCurrency> createConvertCurrency(ConvertCurrency value) {
        return new JAXBElement<ConvertCurrency>(_ConvertCurrency_QNAME, ConvertCurrency.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrenciesByDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrenciesByDate")
    public JAXBElement<GetCurrenciesByDate> createGetCurrenciesByDate(GetCurrenciesByDate value) {
        return new JAXBElement<GetCurrenciesByDate>(_GetCurrenciesByDate_QNAME, GetCurrenciesByDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrenciesByDateAndCC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrenciesByDateAndCC")
    public JAXBElement<GetCurrenciesByDateAndCC> createGetCurrenciesByDateAndCC(GetCurrenciesByDateAndCC value) {
        return new JAXBElement<GetCurrenciesByDateAndCC>(_GetCurrenciesByDateAndCC_QNAME, GetCurrenciesByDateAndCC.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrencyByDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrencyByDate")
    public JAXBElement<GetCurrencyByDate> createGetCurrencyByDate(GetCurrencyByDate value) {
        return new JAXBElement<GetCurrencyByDate>(_GetCurrencyByDate_QNAME, GetCurrencyByDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrenciesWithBaseCCResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrenciesWithBaseCCResponse")
    public JAXBElement<GetCurrenciesWithBaseCCResponse> createGetCurrenciesWithBaseCCResponse(GetCurrenciesWithBaseCCResponse value) {
        return new JAXBElement<GetCurrenciesWithBaseCCResponse>(_GetCurrenciesWithBaseCCResponse_QNAME, GetCurrenciesWithBaseCCResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrenciesWithBaseCC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrenciesWithBaseCC")
    public JAXBElement<GetCurrenciesWithBaseCC> createGetCurrenciesWithBaseCC(GetCurrenciesWithBaseCC value) {
        return new JAXBElement<GetCurrenciesWithBaseCC>(_GetCurrenciesWithBaseCC_QNAME, GetCurrenciesWithBaseCC.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MapElements }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "elemets")
    public JAXBElement<MapElements> createElemets(MapElements value) {
        return new JAXBElement<MapElements>(_Elemets_QNAME, MapElements.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrnecyToPeriodResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrnecyToPeriodResponse")
    public JAXBElement<GetCurrnecyToPeriodResponse> createGetCurrnecyToPeriodResponse(GetCurrnecyToPeriodResponse value) {
        return new JAXBElement<GetCurrnecyToPeriodResponse>(_GetCurrnecyToPeriodResponse_QNAME, GetCurrnecyToPeriodResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrenciesByDateAndCCResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrenciesByDateAndCCResponse")
    public JAXBElement<GetCurrenciesByDateAndCCResponse> createGetCurrenciesByDateAndCCResponse(GetCurrenciesByDateAndCCResponse value) {
        return new JAXBElement<GetCurrenciesByDateAndCCResponse>(_GetCurrenciesByDateAndCCResponse_QNAME, GetCurrenciesByDateAndCCResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrencyByDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = SERVICE_URL, name = "getCurrencyByDateResponse")
    public JAXBElement<GetCurrencyByDateResponse> createGetCurrencyByDateResponse(GetCurrencyByDateResponse value) {
        return new JAXBElement<GetCurrencyByDateResponse>(_GetCurrencyByDateResponse_QNAME, GetCurrencyByDateResponse.class, null, value);
    }

}
