
package epam.soap.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cc.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="cc">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SAR"/>
 *     &lt;enumeration value="SGD"/>
 *     &lt;enumeration value="SKK"/>
 *     &lt;enumeration value="VND"/>
 *     &lt;enumeration value="SIT"/>
 *     &lt;enumeration value="SEK"/>
 *     &lt;enumeration value="CHF"/>
 *     &lt;enumeration value="SYP"/>
 *     &lt;enumeration value="TRY"/>
 *     &lt;enumeration value="TMT"/>
 *     &lt;enumeration value="EGP"/>
 *     &lt;enumeration value="GBP"/>
 *     &lt;enumeration value="USD"/>
 *     &lt;enumeration value="UZS"/>
 *     &lt;enumeration value="TWD"/>
 *     &lt;enumeration value="XOF"/>
 *     &lt;enumeration value="XAU"/>
 *     &lt;enumeration value="XDR"/>
 *     &lt;enumeration value="XAG"/>
 *     &lt;enumeration value="XPT"/>
 *     &lt;enumeration value="XPD"/>
 *     &lt;enumeration value="BYR"/>
 *     &lt;enumeration value="EUR"/>
 *     &lt;enumeration value="UAH"/>
 *     &lt;enumeration value="PLN"/>
 *     &lt;enumeration value="BRL"/>
 *     &lt;enumeration value="TJS"/>
 *     &lt;enumeration value="RUB"/>
 *     &lt;enumeration value="AZN"/>
 *     &lt;enumeration value="AUD"/>
 *     &lt;enumeration value="AMD"/>
 *     &lt;enumeration value="BGN"/>
 *     &lt;enumeration value="CAD"/>
 *     &lt;enumeration value="CLP"/>
 *     &lt;enumeration value="CNY"/>
 *     &lt;enumeration value="HRK"/>
 *     &lt;enumeration value="CYP"/>
 *     &lt;enumeration value="CZK"/>
 *     &lt;enumeration value="DKK"/>
 *     &lt;enumeration value="EEK"/>
 *     &lt;enumeration value="HKD"/>
 *     &lt;enumeration value="HUF"/>
 *     &lt;enumeration value="ISK"/>
 *     &lt;enumeration value="INR"/>
 *     &lt;enumeration value="IRR"/>
 *     &lt;enumeration value="IQD"/>
 *     &lt;enumeration value="ILS"/>
 *     &lt;enumeration value="GEL"/>
 *     &lt;enumeration value="JPY"/>
 *     &lt;enumeration value="KZT"/>
 *     &lt;enumeration value="KRW"/>
 *     &lt;enumeration value="KWD"/>
 *     &lt;enumeration value="KGS"/>
 *     &lt;enumeration value="LBP"/>
 *     &lt;enumeration value="LVL"/>
 *     &lt;enumeration value="LYD"/>
 *     &lt;enumeration value="LTL"/>
 *     &lt;enumeration value="MTL"/>
 *     &lt;enumeration value="MXN"/>
 *     &lt;enumeration value="MNT"/>
 *     &lt;enumeration value="MDL"/>
 *     &lt;enumeration value="NZD"/>
 *     &lt;enumeration value="NOK"/>
 *     &lt;enumeration value="PKR"/>
 *     &lt;enumeration value="PEN"/>
 *     &lt;enumeration value="RON"/>
 *     &lt;enumeration value="DEPRICATED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "cc")
@XmlEnum
public enum CC {

    SAR,
    SGD,
    SKK,
    VND,
    SIT,
    SEK,
    CHF,
    SYP,
    TRY,
    TMT,
    EGP,
    GBP,
    USD,
    UZS,
    TWD,
    XOF,
    XAU,
    XDR,
    XAG,
    XPT,
    XPD,
    BYR,
    EUR,
    UAH,
    PLN,
    BRL,
    TJS,
    RUB,
    AZN,
    AUD,
    AMD,
    BGN,
    CAD,
    CLP,
    CNY,
    HRK,
    CYP,
    CZK,
    DKK,
    EEK,
    HKD,
    HUF,
    ISK,
    INR,
    IRR,
    IQD,
    ILS,
    GEL,
    JPY,
    KZT,
    KRW,
    KWD,
    KGS,
    LBP,
    LVL,
    LYD,
    LTL,
    MTL,
    MXN,
    MNT,
    MDL,
    NZD,
    NOK,
    PKR,
    PEN,
    RON,
    DEPRICATED;

    public String value() {
        return name();
    }

    public static CC fromValue(String v) {
        return valueOf(v);
    }

}
