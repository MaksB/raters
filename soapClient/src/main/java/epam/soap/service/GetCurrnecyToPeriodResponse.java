
package epam.soap.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCurrnecyToPeriodResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCurrnecyToPeriodResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrencyPeriod" type="{http://service.soap.epam/}mapElementsArray" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCurrnecyToPeriodResponse", propOrder = {
    "currencyPeriod"
})
public class GetCurrnecyToPeriodResponse {

    @XmlElement(name = "CurrencyPeriod")
    protected MapElementsArray currencyPeriod;

    /**
     * Gets the value of the currencyPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link MapElementsArray }
     *     
     */
    public MapElementsArray getCurrencyPeriod() {
        return currencyPeriod;
    }

    /**
     * Sets the value of the currencyPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link MapElementsArray }
     *     
     */
    public void setCurrencyPeriod(MapElementsArray value) {
        this.currencyPeriod = value;
    }

}
