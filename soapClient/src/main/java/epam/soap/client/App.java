package epam.soap.client;

import java.time.LocalDate;
import java.util.Arrays;

import epam.soap.service.CC;
import epam.soap.service.impl.CurrencyContrloller;
import epam.soap.service.impl.CurrencyControllerImplService;
import epam.soap.service.impl.Exception;
import epam.soap.service.impl.InformationController;
import epam.soap.service.impl.InformationControllerImplService;


public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	CurrencyControllerImplService controllerImplService = new CurrencyControllerImplService();
    	CurrencyContrloller currencyContrloller = controllerImplService.getCurrencyControllerImplPort();
    	System.out.println("Get currencies by date");
    	currencyContrloller.getCurrenciesByDate(LocalDate.now()).stream().forEach(System.out::println);
    	System.out.println("\nGet currencies by date and cc");
    	currencyContrloller.getCurrenciesByDateAndCC(LocalDate.now(), Arrays.asList(CC.USD, CC.EUR)).stream().forEach(System.out::println);
    	System.out.println("\nGet currencies with base and cc");
    	currencyContrloller.getCurrenciesWithBaseCC(LocalDate.now(), CC.EUR).stream().forEach(System.out::println);
    	System.out.println("\nGet currency by date");
    	System.out.println(currencyContrloller.getCurrencyByDate(LocalDate.now(), CC.SEK));
    	System.out.println("\nGet currencies period");
    	currencyContrloller.getCurrnecyToPeriod(LocalDate.of(2016, 04, 01), LocalDate.of(2016,04,20)).getItem().stream().forEach(e -> {
    		System.out.println("Date "+e.getDate());
    		e.getCurrencies().getCurrency().stream().forEach(System.out::println);
    	});;
  	    System.out.println("\nConvert currency");
    	System.out.println(currencyContrloller.convertCurrency(CC.EUR, CC.USD, 200F));
    	InformationControllerImplService informationControllerImplService = new InformationControllerImplService();
    	InformationController informationController = informationControllerImplService.getInformationControllerImplPort();
    	System.out.println("\nGet information about currency");
    	informationController.getAllInformationAboutCurrency().stream().forEach(System.out::println);
    	System.out.println("\nGet information about CC.CAD");
    	System.out.println(informationController.getInformationAboutCurrency(CC.CAD));
    }
}
