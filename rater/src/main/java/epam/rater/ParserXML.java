package epam.rater;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.EnumMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import epam.rater.model.CC;
import epam.rater.model.Currency;
import epam.rater.model.Exchange;

public class ParserXML implements Parser{

	@Override
	public   Map<CC, Currency> parse(LocalDate localDate) {
		Map<CC, Currency> currencyMap = new EnumMap<>(CC.class);
		try {
			URL url = new URL(
					"http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date="
							+ localDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
			parseXML(url.openStream(), currencyMap);
		} catch (JAXBException | IOException e) {
			e.printStackTrace();
		}
		return currencyMap;
	}
	
	private void parseXML(InputStream inputStream, Map<CC, Currency> currencyMap) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(Exchange.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Exchange exchange = (Exchange) jaxbUnmarshaller.unmarshal(inputStream);
		exchange.getCurrency().stream().filter(c -> c.getCc() == null).forEach(c -> c.setCc(CC.DEPRICATED));
		exchange.getCurrency().stream().forEach(e ->  currencyMap.put(e.getCc() , e));
		
	}
}
