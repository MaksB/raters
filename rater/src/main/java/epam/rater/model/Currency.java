package epam.rater.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@XmlRootElement(name = "currency")
public class Currency implements Serializable{

	private static final long serialVersionUID = 1L;

	
	private String name;
	
	private Integer code;

	private Date exchangedate;
	private Float rate;
	private CC cc;
	
	public Currency() {
	}
	
	@XmlElement(name = "txt")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name = "r030")
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	@XmlJavaTypeAdapter(type = Date.class, value = DateAdapter.class)
	public Date getExchangedate() {
		return exchangedate;
	}

	public void setExchangedate(Date exchangedate) {
		this.exchangedate = exchangedate;
	}

	public Float getRate() {
		return rate;
	}

	public void setRate(Float rate) {
		this.rate = rate;
	}

	public CC getCc() {
		return cc;
	}

	public void setCc(CC cc) {
		this.cc = cc;
	}

	@Override
	public String toString() {
		return "Currency [name=" + name + ", exchangedate="
				+ exchangedate + ", rate=" + rate + ", cc=" + cc + "]";
	}

}
