package epam.rater.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "exchange")
@XmlAccessorType(XmlAccessType.FIELD)
public class Exchange {

	@XmlElement(name = "currency")
	private List<Currency> currency;

	public List<Currency> getCurrency() {
		return currency;
	}

	public void setCurrency(List<Currency> currency) {
		this.currency = currency;
	}
	
	
}
