package epam.rater.model;

public class Converter {

	private Currency base;
	private Currency secondary;
	private Float amount;
	private Float rate;

	public Converter() {
	}
	
	public Currency getBase() {
		return base;
	}

	public void setBase(Currency base) {
		this.base = base;
	}

	public Currency getSecondary() {
		return secondary;
	}

	public void setSecondary(Currency secondary) {
		this.secondary = secondary;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float value) {
		this.amount = value;
	}

	public Float getRate() {
		return rate;
	}

	public void setRate(Float rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return "Converter [base=" + base + ", secondary=" + secondary + ", amount=" + amount + ", rate=" + rate + "]";
	}

}
