package epam.rater.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import epam.rater.model.CC;
import epam.rater.model.Converter;
import epam.rater.model.Currency;

public interface RateService {

	public List<Currency> getAllCurrencyByDate(LocalDate localDate);
	public Currency getCurrencyByDate(LocalDate localDate, CC cc);
	public List<Currency> getCurrenciesByDateAndCC(LocalDate localDate, List<CC> cc);
	public List<Currency> getAllCurrencyWithBaseCC(LocalDate localDate, CC cc);
	public Converter convertCurrency(CC base, CC newCC, Float mount);
	public Map<LocalDate, List<Currency>> getCurrnecyToPeriod(LocalDate startDate, LocalDate endDate);
	public void validateDatePeriod(LocalDate... date);
	
}
