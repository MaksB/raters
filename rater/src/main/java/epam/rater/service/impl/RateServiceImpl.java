package epam.rater.service.impl;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import epam.custom.exception.CustomExeptionValidate;
import epam.rater.flyweight.FlyweightCurrency;
import epam.rater.model.CC;
import epam.rater.model.Converter;
import epam.rater.model.Currency;
import epam.rater.service.RateService;

public class RateServiceImpl implements RateService {

	@Override
	public List<Currency> getAllCurrencyByDate(LocalDate localDate) {
		return FlyweightCurrency.getCurrencyByDate(localDate);
	}

	@Override
	public Currency getCurrencyByDate(LocalDate localDate, CC cc) {
		return FlyweightCurrency.getCurrencyByDateAndCC(localDate, cc);
	}

	@Override
	public List<Currency> getCurrenciesByDateAndCC(LocalDate localDate, List<CC> listCC) {
		return FlyweightCurrency.getListCurrency(localDate, listCC);
	}

	@Override
	public List<Currency> getAllCurrencyWithBaseCC(LocalDate localDate, CC baseCC) {
		Currency baseCurrency = FlyweightCurrency.getCurrencyByDateAndCC(localDate, baseCC);
		List<Currency> currencies = FlyweightCurrency.getCurrencyByDate(localDate);
		return convertBaseCurrency(currencies, baseCurrency);
	}

	private List<Currency> convertBaseCurrency(List<Currency> currencies, Currency baseCurrency) {
		currencies.stream().forEach(c -> c.setRate(c.getRate() / baseCurrency.getRate()));
		return currencies;
	}

	@Override
	public Converter convertCurrency(CC base, CC newCC, Float amount) {
		Currency currencyBase = FlyweightCurrency.getCurrencyByDateAndCC(LocalDate.now(), base);
		Currency currencyNew = FlyweightCurrency.getCurrencyByDateAndCC(LocalDate.now(), newCC);

		Float rate = currencyNew.getRate() / currencyBase.getRate();
		Float newAmount = amount / rate;

		Converter converter = new Converter();
		converter.setBase(currencyBase);
		converter.setSecondary(currencyNew);
		converter.setAmount(newAmount);
		converter.setRate(rate);

		return converter;
	}

	@Override
	public Map<LocalDate, List<Currency>> getCurrnecyToPeriod(LocalDate startDate, LocalDate endDate) {
		Map<LocalDate, List<Currency>> currencies = new HashMap<>();

		long period = ChronoUnit.DAYS.between(startDate, endDate);
		for (long i = 0; i < period; i++) {
			currencies.put(startDate, FlyweightCurrency.getCurrencyByDate(startDate));
			startDate = startDate.plusDays(1);
		}
		return currencies;
	}

	@Override
	public void validateDatePeriod(LocalDate... date) {

		Arrays.asList(date).stream().forEach(d -> {
			if (d.isAfter(LocalDate.now()) || d.isBefore(LocalDate.of(1997, 1, 1))) {
				throw new CustomExeptionValidate();
			}
		});

	}
}
