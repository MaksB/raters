package epam.rater;

import java.time.LocalDate;
import java.util.Map;

import epam.rater.model.CC;
import epam.rater.model.Currency;

@FunctionalInterface
public interface Parser {
	public Map<CC, Currency> parse(LocalDate localDate);
}
