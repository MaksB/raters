package epam.rater;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import epam.rater.model.CC;
import epam.rater.model.Currency;

public class ParserXLS implements Parser{

	private static final String DD_MM_YYYY = "dd.MM.yyyy";

	@Override
	public Map<CC, Currency> parse(LocalDate localDate) {
		Map<CC, Currency> currencyMap = new EnumMap<>(CC.class);
		try {
			URL url = new URL(
					"http://www.bank.gov.ua/control/uk/curmetal/currency/search?formType=searchFormDate&time_step=daily&date="
							+ localDate.format(DateTimeFormatter.ofPattern(DD_MM_YYYY))
							+ "&outer=xls&execute=%D0%92%D0%B8%D0%BA%D0%BE%D0%BD%D0%B0%D1%82%D0%B8");

	           getXLSFile(url.openStream(), currencyMap);
			url = new URL(
					"http://www.bank.gov.ua/control/uk/curmetal/currency/search?formType=searchFormDate&time_step=monthly&date="
							+ localDate.format(DateTimeFormatter.ofPattern(DD_MM_YYYY))
							+ "&outer=xls&execute=%D0%92%D0%B8%D0%BA%D0%BE%D0%BD%D0%B0%D1%82%D0%B8");
			getXLSFile(url.openStream(), currencyMap);
		} catch (IOException e) {
			currencyMap.clear();
			currencyMap =  new ParserXML().parse(localDate);
		}
		return currencyMap;
	}

	private void getXLSFile(InputStream inputStream, Map<CC, Currency> currencyMap) throws IOException {
	
		HSSFWorkbook wb = new HSSFWorkbook(inputStream);
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFRow row;

		Iterator rows = sheet.rowIterator();
		rows.next();
		while (rows.hasNext()) {
			row = (HSSFRow) rows.next();
			Currency currency = initCurrency(row);
			currencyMap.put(currency.getCc(), currency);
		}
	}

	private Currency initCurrency(HSSFRow row) {
		Currency currency = new Currency();
		currency.setCc(convertToCC(row.getCell(0).getStringCellValue()));
		currency.setName(row.getCell(0).getStringCellValue().replaceAll(".{5}$", ""));
		SimpleDateFormat dateFormat = new SimpleDateFormat(DD_MM_YYYY);
		try {
			currency.setExchangedate(dateFormat.parse(row.getCell(1).getStringCellValue()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		currency.setRate((float) (row.getCell(4).getNumericCellValue() / row.getCell(3).getNumericCellValue()));
		return currency;
	}

	private CC convertToCC(String nameCurrency) {
		String ccName = nameCurrency.substring(nameCurrency.indexOf("("), nameCurrency.indexOf(")")).replace("(", "");
		try {
			return CC.valueOf(ccName);
		} catch (Exception e) {
			return CC.DEPRICATED;
		}
		
	}

}
