package epam.rater.flyweight;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.SerializationUtils;

import epam.rater.Parser;
import epam.rater.ParserXLS;
import epam.rater.model.CC;
import epam.rater.model.Currency;

public class FlyweightCurrency {

	private static Map<LocalDate, Map<CC, Currency>> currencyStore = new HashMap<>();
	private static Parser parserXLS = new ParserXLS();

	public static List<Currency> getCurrencyByDate(LocalDate localDate) {
		if (!currencyStore.containsKey(localDate)) {
			currencyStore.put(localDate, parserXLS.parse(localDate));
		}
		return currencyStore.get(localDate).values().stream().map(c -> (Currency)SerializationUtils.clone(c)).collect(Collectors.toList());
	
	}

	public static Currency getCurrencyByDateAndCC(LocalDate localDate, CC cc) {
		if (!currencyStore.containsKey(localDate)) {
			currencyStore.put(localDate, parserXLS.parse(localDate));
		}
		return (Currency)SerializationUtils.clone(currencyStore.get(localDate).get(cc));
	}

	public static List<Currency> getListCurrency(LocalDate localDate, List<CC> listCC) {
		List<Currency> currencies = new ArrayList<>();
		if (!currencyStore.containsKey(localDate)) {
			currencyStore.put(localDate, parserXLS.parse(localDate));
		}
	
		listCC.stream().forEach(c ->currencies.add(currencyStore.get(localDate).get(c)));
		return currencies.stream().map(c ->(Currency)SerializationUtils.clone(c) ).collect(Collectors.toList());
	}

}
