package epam.custom.exception;

import javax.xml.ws.WebFault;

@WebFault(name = "CustomExeptionValidate")
public class CustomExeptionValidate extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomExeptionValidate() {
	}
	
	@Override
	public String getMessage() {
		return "wrong date period. Normal period 01.01.1997-now";
	}

}
